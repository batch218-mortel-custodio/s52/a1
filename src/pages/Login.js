
import {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login() {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);


	function registerUser(e){
		e.preventDefault()

		setEmail("");
		setPassword("");
		alert("Login successful!");
	}


	useEffect(() => {
		if (email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, password])

    return (
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" value={email} 
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" value={password}
	                placeholder="Password"
	                onChange={e => setPassword(e.target.value)} 
	                required
                />
            </Form.Group>

            	{/*// ternary operator*/}
            	{ isActive ? 

            	<Button variant="primary" type="submit" id="submitBtnLogin">
            		Login
            	</Button>
            	:
            	<Button variant="danger" type="submit" id="submitBtnLogin" disabled>
            		Login
            	</Button>     	
            	}


        </Form>
    )

}